vim.wo.number = true
vim.wo.relativenumber = true
vim.o.mouse = ''
vim.cmd('colorscheme catppuccin-mocha')
vim.wo.signcolumn = 'yes'
