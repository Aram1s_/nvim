local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
vim.opt.rtp:prepend(lazypath)

local plugins = {
	"nvim-treesitter/nvim-treesitter",
	"neovim/nvim-lspconfig",
	"hrsh7th/nvim-cmp",
	"hrsh7th/cmp-nvim-lsp",
	{
		"nvim-telescope/telescope.nvim",
		dependencies = { "nvim-lua/plenary.nvim" }
	},
	{ "numToStr/Comment.nvim", opts = {} },
	{ "j-hui/fidget.nvim",     event = "LspAttach", opts = {} },
	{ "catppuccin/nvim",       name = "catppuccin" },
	"andweeb/presence.nvim",
}

require("lazy").setup(plugins, opts)
